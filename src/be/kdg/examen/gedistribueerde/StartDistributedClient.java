package be.kdg.examen.gedistribueerde;

import be.kdg.examen.gedistribueerde.client.Client;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communications.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;
import be.kdg.examen.gedistribueerde.server.ServerStub;

public class StartDistributedClient {
    public static void main(String[] args) {
        Server server = new ServerStub(new NetworkAddress("192.168.1.60",53829));
        DocumentImpl document = new DocumentImpl();
        Client client = new Client(server,document);
        client.run();
    }
}
