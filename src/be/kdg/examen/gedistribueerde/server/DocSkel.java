package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.communications.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communications.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communications.communication.NetworkAddress;

public class DocSkel implements Runnable{
    private final MessageManager messageManager;
    private final Document doc;

    public DocSkel(Document doc) {
        this.messageManager = new MessageManager();
        this.doc = doc;
    }

    private void handleRequest(MethodCallMessage request) {
        String methodName = request.getMethodName();
        if ("append".equals(methodName)) {
            handleAppend(request);
        }
    }

    private void handleAppend(MethodCallMessage request) {
        char nextChar = request.getParameter("char").charAt(0);
        doc.append(nextChar);
        sendEmptyReply(request);
    }

    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }
    public NetworkAddress getAddress(){
        return messageManager.getMyAddress();
    }
    public void run() {
        System.out.println("started Document Skeleton.");
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }
}
