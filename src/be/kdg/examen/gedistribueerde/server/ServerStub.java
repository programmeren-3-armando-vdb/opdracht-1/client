package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communications.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communications.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communications.communication.NetworkAddress;

import java.lang.reflect.Method;

public class ServerStub implements Server {
    private final NetworkAddress networkAddress;
    private final MessageManager messageManager;

    public ServerStub(NetworkAddress networkAddress) {
        this.networkAddress = networkAddress;
        this.messageManager = new MessageManager();
    }

    @Override
    public void log(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "log");
        message.setParameter("text", document.getText());
        messageManager.send(message, networkAddress);
        checkEmptyReply();
    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
        System.out.println("Received OK reply.");
    }

    @Override
    public Document create(String s) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "create");
        message.setParameter("s", s);
        Document doc = new DocumentImpl();
        messageManager.send(message, networkAddress);
        MethodCallMessage response = messageManager.wReceive();
        doc.setText(response.getParameter("docText"));
        return doc;
    }

    @Override
    public void toUpper(Document document) {
        textManipulation(document, "toUpper");
    }

    private void textManipulation(Document document, String methodName) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), methodName);
        message.setParameter("docText", document.getText());
        messageManager.send(message, networkAddress);
        MethodCallMessage response = messageManager.wReceive();
        document.setText(response.getParameter("docText"));
    }

    @Override
    public void toLower(Document document) {
        textManipulation(document, "toLower");
    }

    @Override
    public void type(Document document, String text) {
        DocSkel docSkel = new DocSkel(document);
        Thread skeletonThread = new Thread(docSkel);
        skeletonThread.start();
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"type");
        message.setParameter("ip",docSkel.getAddress().getIpAddress());
        message.setParameter("port", String.valueOf(docSkel.getAddress().getPortNumber()));
        message.setParameter("docText", text);
        messageManager.send(message, networkAddress);
    }
}
